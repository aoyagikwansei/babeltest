'use strict';

class Character {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.health_ = 100;
    }
    attack(character) {
        this.x += 10;
        character.x -= 10;
        character.health_ -= 10;
    }
    get health() {
        return this.health_;
    }
}